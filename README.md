# OpenCV face detection

OpenCV comes with a trainer as well as detector.  
If you want to train your own classifier for any object like car, planes etc. you can use OpenCV to create one.  

## Image face detection
- Run :`python face_detection_image.py`
- Inspiration: https://docs.opencv.org/trunk/d7/d8b/tutorial_py_face_detection.html

## Video face detection
- Run: `python face_detection_video.py`
- Inspiration: https://pythonprogramming.net/haar-cascade-face-eye-detection-python-opencv-tutorial/


## Haar Cascades pre-trained classifiers
OpenCV already contains many pre-trained classifiers for face, eyes, smiles, etc.  
Those XML files are stored in the opencv/data/haarcascades/ folder.  
- [Download](https://github.com/opencv/opencv/tree/master/data/haarcascades)

```
ls -1 /usr/share/opencv4/lbpcascades/
lbpcascade_frontalcatface.xml
lbpcascade_frontalface_improved.xml
lbpcascade_frontalface.xml
lbpcascade_profileface.xml
lbpcascade_silverware.xml

ls -1 /usr/share/opencv4/haarcascades/
haarcascade_eye_tree_eyeglasses.xml
haarcascade_eye.xml
haarcascade_frontalcatface_extended.xml
haarcascade_frontalcatface.xml
haarcascade_frontalface_alt2.xml
haarcascade_frontalface_alt_tree.xml
haarcascade_frontalface_alt.xml
haarcascade_frontalface_default.xml
haarcascade_fullbody.xml
haarcascade_lefteye_2splits.xml
haarcascade_licence_plate_rus_16stages.xml
haarcascade_lowerbody.xml
haarcascade_profileface.xml
haarcascade_righteye_2splits.xml
haarcascade_russian_plate_number.xml
haarcascade_smile.xml
haarcascade_upperbody.xml
```