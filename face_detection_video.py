import numpy as np
import cv2 as cv
import os

face_cascade = cv.CascadeClassifier(os.path.join('haarcascades', 'haarcascade_frontalface_default.xml'))
eye_cascade = cv.CascadeClassifier(os.path.join('haarcascades', 'haarcascade_eye_tree_eyeglasses.xml'))

cap = cv.VideoCapture(0)

while 1:
    ret, img = cap.read()
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    for (x,y,w,h) in faces:
        cv.rectangle(img,(x,y),(x+w,y+h),(255,0,0),5)
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = img[y:y+h, x:x+w]

        eyes = eye_cascade.detectMultiScale(roi_gray)
        for (ex,ey,ew,eh) in eyes:
            cv.rectangle(roi_color,(ex,ey),(ex+ew,ey+eh),(0,255,0),10)

    cv.imshow('img',img)
    k = cv.waitKey(30) & 0xff
    if k == 27:
        break

cap.release()
cv.destroyAllWindows() 
